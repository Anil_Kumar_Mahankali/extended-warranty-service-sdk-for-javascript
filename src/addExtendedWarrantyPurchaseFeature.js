import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AddExtendedWarrantyDraftReq from './addExtendedWarrantyDraftReq';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import ExtendedWarrantyId from './extendedWarrantyId';

@inject(ExtendedWarrantyServiceSdkConfig, HttpClient)
class AddExtendedWarrantyPurchaseFeature {

    _config:ExtendedWarrantyServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ExtendedWarrantyServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * @param request {AddExtendedWarrantyDraftReq}
     * @param accessToken {string}
     * @returns {Promise.<ExtendedWarrantyId>}
     */
    execute(request:AddExtendedWarrantyDraftReq,
            accessToken:string):Promise<ExtendedWarrantyId> {

        return this._httpClient
            .createRequest('extended-warranty')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default AddExtendedWarrantyPurchaseFeature;