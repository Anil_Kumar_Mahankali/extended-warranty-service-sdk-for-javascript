/**
 * @class {ExtendedWarrantySimpleLineItem}
 */
export default class ExtendedWarrantySimpleLineItem{

    _id:number;

    _assetId:string;

    _serialNumber:string;

    _productLineName:string;

    _terms:string;

    _price:number;

    _selectedTerms:string;

    _selectedPrice:number;

    _materialNumber:string;

    _productName:string;


    /**
     * @param {number} id
     * @param {string} assetId
     * @param {string} serialNumber
     * @param {string} productLineName
     * @param {string} terms
     * @param {number} price
     * @param {string} selectedTerms
     * @param {number} selectedPrice
     * @param {string} materialNumber
     * @param {string} productName
     */
    constructor(id:number,
                assetId:string,
                serialNumber:string,
                productLineName:string,
                terms:string,
                price:number,
                selectedTerms:string,
                selectedPrice:number,
                materialNumber:string,
                productName:string
    ){
        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!assetId){
            throw new TypeError('assetId required');
        }
        this._assetId = assetId;

        if(!serialNumber){
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        this._productLineName = productLineName;

        this._terms = terms;

        this._price = price;

        this._selectedTerms = selectedTerms;

        this._selectedPrice = selectedPrice;

        if(!materialNumber){
            throw new TypeError('materialNumber required');
        }
        this._materialNumber = materialNumber;
        
        this._productName = productName;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get assetId():string{
        return this._assetId;
    }

    get serialNumber():string{
        return this._serialNumber;
    }

    get productLineName():string{
        return this._productLineName;
    }

    get terms():string{
        return this._terms;
    }

    get price():number{
        return this._price;
    }

    get selectedTerms():string{
        return this._selectedTerms;
    }

    get selectedPrice():number{
        return this._selectedPrice;
    }

    get materialNumber():string{
        return this._materialNumber;
    }

    get productName():string{
        return this._productName;
    }

    toJSON(){

        return {
            id:this._id,
            assetId:this._assetId,
            serialNumber:this._serialNumber,
            productLineName:this._productLineName,
            terms:this._terms,
            price:this._price,
            selectedTerms:this._selectedTerms,
            selectedPrice:this._selectedPrice,
            materialNumber:this._materialNumber,
            productName:this._productName
        }

    }
}
