import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ExtendedWarrantyServiceSdkConfig from './extendedWarrantyServiceSdkConfig';
import AddExtendedWarrantyPurchaseFeature from './addExtendedWarrantyPurchaseFeature';
import ListPartnerSaleRegDraftsWithPartnerAccountIdFeature from './listPartnerSaleRegDraftsWithPartnerAccountIdFeature';
import GetExtendedWarrantyPurchaseFeature from './getExtendedWarrantyPurchaseFeature';
import CheckDiscountAvailOnExtendedWarrantyFeature from './checkDiscountAvailOnExtendedWarrantyFeature';
import SubmitExtendedWarrantyDraftFeature from './submitExtendedWarrantyDraftFeature';
import UpdateExtendedWarrantyDraftFeature from './updateExtendedWarrantyDraftFeature';


/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {ExtendedWarrantyServiceSdkConfig} config
     */
    constructor(config:ExtendedWarrantyServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(ExtendedWarrantyServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddExtendedWarrantyPurchaseFeature);
        this._container.autoRegister(ListPartnerSaleRegDraftsWithPartnerAccountIdFeature);
        this._container.autoRegister(GetExtendedWarrantyPurchaseFeature);
        this._container.autoRegister(CheckDiscountAvailOnExtendedWarrantyFeature);
        this._container.autoRegister(SubmitExtendedWarrantyDraftFeature);
        this._container.autoRegister(UpdateExtendedWarrantyDraftFeature);

    }

}
