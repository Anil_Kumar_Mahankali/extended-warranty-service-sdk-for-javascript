import ExtendedWarrantyServiceSdk,{
    AddExtendedWarrantyDraftReq,
    PartnerCommercialSaleRegSynopsisView,
    ExtendedWarrantyDraftRes,
    ExtendedWarrantySubmitReq,
    UpdateExtendedWarrantyDraftReq
} from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be ExtendedWarrantyServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ExtendedWarrantyServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('listSubmittedPartnerCommercialSaleRegDraftWithAccountId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);


                /*
                 act
                 */
                const partnerCommercialSaleRegDraftWithAccountIdPromise =
                    objectUnderTest
                        .listSubmittedPartnerCommercialSaleRegDraftWithAccountId(
                            dummy.partnerAccountId,
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                partnerCommercialSaleRegDraftWithAccountIdPromise
                    .then(PartnerCommercialSaleRegSynopsisView => {
                        expect(PartnerCommercialSaleRegSynopsisView.length >= 1).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        /*describe('addPartnerCommercialSaleRegDraft method', () => {
            it('should return id', (done) => {
                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);

                let addExtendedWarrantyDraftReq =
                    new AddExtendedWarrantyDraftReq(
                        dummy.partnerSaleRegistrationId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.isSubmitted,
                        dummy.discountCode,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /!*
                 act
                 *!/
                const actPromise =
                    objectUnderTest
                        .addExtendedWarrantyPurchase(
                            addExtendedWarrantyDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /!*
                 assert
                 *!/
                actPromise
                    .then(ExtendedWarrantyId => {
                        expect(ExtendedWarrantyId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });
*/
        /*describe('updateExtendedWarrantyPurchase method', () => {
            it('should return id', (done) => {
                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);

                let addExtendedWarrantyDraftReq =
                    new AddExtendedWarrantyDraftReq(
                        dummy.partnerSaleRegistrationId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.isSubmitted,
                        dummy.discountCode,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /!*
                 act
                 *!/
                const addPromise =
                    objectUnderTest
                        .addExtendedWarrantyPurchase(
                            addExtendedWarrantyDraftReq,
                            factory.constructValidAppAccessToken()
                        );

                /!*
                 act
                 *!/
                const actPromise =
                    addPromise
                        .then(ExtendedWarrantyId =>
                            objectUnderTest
                                .updateExtendedWarrantyPurchase(
                                    ExtendedWarrantyId.id,
                                    new UpdateExtendedWarrantyDraftReq(
                                        ExtendedWarrantyId.id,
                                        dummy.partnerSaleRegistrationId,
                                        dummy.facilityName,
                                        dummy.partnerAccountId,
                                        dummy.isSubmitted,
                                        dummy.discountCode,
                                        factory.constructSimpleLineItems(),
                                        factory.constructCompositeLineItems()
                                    ),
                                    factory.constructValidAppAccessToken()
                                )
                        );

                /!*
                 assert
                 *!/
                actPromise
                    .then(ExtendedWarrantyId.id => {
                        expect(ExtendedWarrantyId.id).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });*/

        /*describe('submitExtendedWarrantyDraft method', () => {
            it('should return ExtendedWarrantyDraftRes', (done) => {

                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);

                let addExtendedWarrantyDraftReq =
                    new AddExtendedWarrantyDraftReq(
                        dummy.partnerSaleRegistrationId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.isSubmitted,
                        dummy.discountCode,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /!*
                 act
                 *!/
                const addPromise =
                    objectUnderTest
                        .addExtendedWarrantyPurchase(
                            addExtendedWarrantyDraftReq,
                            factory.constructValidAppAccessToken()
                        );

                /!*
                 act
                 *!/
                const actPromise =
                    addPromise
                        .then(ExtendedWarrantyId =>
                                objectUnderTest
                                    .submitExtendedWarrantyDraft(
                                        new ExtendedWarrantySubmitReq(
                                            ExtendedWarrantyId.id,
                                            dummy.partnerSaleRegistrationId,
                                            dummy.purchaseOrder,
                                            dummy.partnerAccountId,
                                            dummy.sapAccountNumber,
                                            dummy.totalPrice,
                                            dummy.discountCode,
                                            dummy.discountAppliedPrice
                                        ),
                                        factory.constructValidAppAccessToken()
                                    )
                        );

                /!*
                 assert
                 *!/
                actPromise
                    .then((ExtendedWarrantyDraftRes) => {
                        expect(ExtendedWarrantyDraftRes).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });*/

        /*describe('getExtendedWarrantyPurchase method', () => {
            it('should return ExtendedWarrantyDraftRes', (done) => {

                /!*
                 arrange
                 *!/
                const objectUnderTest =
                    new ExtendedWarrantyServiceSdk(config.extendedWarrantyServiceSdkConfig);

                let addExtendedWarrantyDraftReq =
                    new AddExtendedWarrantyDraftReq(
                        dummy.partnerSaleRegistrationId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.isSubmitted,
                        dummy.discountCode,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /!*
                 act
                 *!/
                const actPromise =
                    objectUnderTest
                        .addExtendedWarrantyPurchase(
                            addExtendedWarrantyDraftReq,
                            factory.constructValidAppAccessToken()
                        );

                const submitPromise =
                    actPromise
                        .then(ExtendedWarrantyId =>
                            objectUnderTest
                                .submitExtendedWarrantyDraft(
                                    new ExtendedWarrantySubmitReq(
                                        ExtendedWarrantyId.id,
                                        dummy.partnerSaleRegistrationId,
                                        dummy.purchaseOrder,
                                        dummy.partnerAccountId,
                                        dummy.sapAccountNumber,
                                        dummy.totalPrice,
                                        dummy.discountCode,
                                        dummy.discountAppliedPrice
                                    ),
                                    factory.constructValidAppAccessToken()
                                )
                        );

                const getExtendedWarrantyPurchasePromise =
                    submitPromise
                         .then(()=>
                             objectUnderTest
                                 .getExtendedWarrantyPurchase(
                                    dummy.partnerSaleRegistrationId,
                                    factory
                                        .constructValidAppAccessToken()
                                )
                        );

                /!*
                 assert
                 *!/
                getExtendedWarrantyPurchasePromise
                    .then(ExtendedWarrantyDraftRes => {
                        expect(ExtendedWarrantyDraftRes).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });*/

    });
});



