/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    extendedWarrantyId:'EW000002',
    purchaseOrder:'PO123',
    partnerSaleRegistrationId:1010,
    firstName: 'DummyFirstName',
    lastName: 'DummyLastName',
    facilityId:'001A000001MqdaIIAR',
    facilityName:"TestFacility",
    partnerAccountId: "001A000000MGmjvIAD",
    discountCode : "TGH3G",
    isSubmitted:false,
    url: 'https://dummy-url.com',
    sapAccountNumber:'163687',
    assetId:"123",
    serialNumber:"1234567",
    productLineName:"line1",
    terms:"E/1",
    price:200,
    selectedTerms:"E/2",
    selectedPrice:100,
    totalPrice: 200,
    discountAppliedPrice : 10,
    materialNumber: "dummyMaterialNumber",
    productName:"dummyProductName"
};



